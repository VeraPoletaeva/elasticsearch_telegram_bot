<?php

namespace App\Telegram;

use App\Elasticsearch\Elasticsearch;

/**
 * Класс для работы с телеграмм-ботом.
 */
class TelegramBot
{
  protected $token = '';
  protected $updates = [];
  protected $chat_id = '';
  protected $user_id = '';

  /**
   * Конструктор класса.
   */
  public function __construct($updates)
  {
    $conf = require_once '../config/conf.php';
    $this->token = $conf['telegram_token'];
    $this->updates = $updates;
    $this->chat_id = $this->updates['message']['chat']['id'];
    $this->user_id = $this->updates['message']['from']['id'];
    return $conf;
  }

  /**
   * Основной метод телеграм-бота.
   * Обрабатывает полученные сообщения и запускает соответствующие команды.
   * 
   * @return void
   */
  public function update()
  {
    $message = $this->updates['message']['text'];

    switch ($message) {
      case "/start":
        $this->startCommand();
        break;
      case "/help":
        $this->helpCommand();
        break;
      default:
        $command = TelegramParser::getCommand($message);

        if (!$command) {
          $this->sendMessage('Wrong command');
        }

        if ($command['command'] == 'search') {
          $this->searchCommand($command['text']);
        } elseif ($command['command'] == 'remove') {
          $this->removeCommand($command['text']);
        } elseif ($command['command'] == 'add') {
          $this->addCommand($command['text']);
        } else {
          $this->sendMessage('Wrong command');
        }

        break;
    }
  }

  /**
   * Метод для отправки сообщений.
   */
  private function sendMessage(string $message)
  {
    $result = file_get_contents(
      'https://api.telegram.org/bot' . $this->token
        . '/sendMessage?chat_id=' . $this->chat_id
        . '&text=' . $message
    );
  }

  /**
   * Команда-приветствие.
   * 
   * @return void
   */
  private function startCommand()
  {
    $message = "Welcome to Elasticsearch telegram bot!"
      . "%0A%0ATo search for text, type \"search: <word/text>\""
      . "%0ATo remove text, type \"remove: <id>\""
      . "%0ATo add text, type \"add: <text>\"";
    $this->sendMessage($message);
  }

  /**
   * Команда-справка.
   * 
   * @return void
   */
  private function helpCommand()
  {
    $message = "Help message to Elasticsearch telegram bot!"
      . "%0A%0ATo search for text, type \"search: <word/text>\""
      . "%0ATo remove text, type \"remove: <id>\""
      . "%0ATo add text, type \"add: <text>\"";
    $this->sendMessage($message);
  }

  /**
   * Команда-поиск по тексту или ID (2 в 1) в elasticsearch.
   * 
   * @return void
   */
  private function searchCommand(string $text)
  {
    $this->sendMessage('Searching for text');

    $message = "";

    if (strlen($text) > 0) {
      $elastic = new Elasticsearch;
      $idResult = $elastic->searchById($text);
      $textResults = $elastic->searchByText($text);

      $message .= "Results of search by ID:";
      if ($idResult) {
        $message .= "%0AID: " . $idResult['id'] . "%0AText:%0A" . $idResult['text'];
      } else {
        $message .= " Not found";
      }

      $message .= "%0AResults of search by text:";
      if (count($textResults)) {
        foreach ($textResults as $result) {
          $message .= "%0AID: " . $result['id'];
          $message .= "%0AText:%0A" . $result['text'];
        }
      } else {
        $message .= " Not found";
      }
    } else {
      $message = "You have not entered any text";
    }

    $this->sendMessage($message);
  }

  /**
   * Команда добавления текста в elasticsearch.
   * 
   * @return void
   */
  private function addCommand($text)
  {
    $this->sendMessage('Adding text');

    $elastic = new Elasticsearch;
    $res = $elastic->add($this->user_id, $text);

    if (strlen($res))
      $this->sendMessage('Text added successfully. ID: ' . $res);
    else
      $this->sendMessage('Add failed');
  }

  /**
   * Команда удаления текста из elasticsearch.
   * 
   * @return void
   */
  private function removeCommand(string $data_id)
  {
    $this->sendMessage('Removing text');

    $elastic = new Elasticsearch;
    $res = $elastic->remove($data_id, $this->user_id);

    if (strlen($res) > 0)
      $this->sendMessage('Text removed successfully for ID: ' . $res);
    else
      $this->sendMessage('Remove failed');
  }
}
