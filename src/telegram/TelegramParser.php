<?php

namespace App\Telegram;

/**
 * Класс-парсер для сообщений из телеграмма.
 */
class TelegramParser
{
  /**
   * Выделяет название команды и текст к этой команде.
   * 
   * @param string $str
   * 
   * @return array
   */
  public static function getCommand($str)
  {
    $str = trim($str);

    if (stripos($str, 'search:') === 0)
    {
      $str = str_ireplace('search:', '', $str);
      return [
        'command' => 'search', 
        'text' => TelegramParser::clear($str)
      ];
    } elseif (stripos($str, 'add:') === 0) {
      $str = str_ireplace('add:', '', $str);
      return [
        'command' => 'add', 
        'text' => TelegramParser::clear($str)
      ];
    } elseif (stripos($str, 'remove:') === 0) {
      $str = str_ireplace('remove:', '', $str);
      return [
        'command' => 'remove', 
        'text' => TelegramParser::clear($str)
      ];
    } else {
      return [];
    }
  }

  /**
   * Очищает строку от лишних символов.
   * 
   * @param string $str
   * 
   * @return string
   */
  public static function clear(string $str)
  {
    $str = trim($str);
    $str = preg_replace('/\n/', '', $str);
    $str = preg_replace('/ {2,}/', ' ', $str);

    return $str;
  }
}