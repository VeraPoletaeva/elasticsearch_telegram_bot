<?php

namespace App\Elasticsearch;

interface ElasticsearchInterface
{
  public function add(int $user_id, string $text);

  public function remove(string $id, int $user_id);

  public function searchById(string $id);

  public function searchByText(string $text);
}