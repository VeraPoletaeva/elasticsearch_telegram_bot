<?php

namespace App\Elasticsearch;

use Elasticsearch\ClientBuilder;

/**
 * Класс для работы с elasticsearch
 */
class Elasticsearch implements ElasticsearchInterface
{
  protected $client;

  /**
   * Конструктор класса
   */
  public function __construct()
  {
    $hosts = require_once '../../config/conf.php'['es_hosts'];
    $this->client = ClientBuilder::create()->setHosts($hosts)->build();
    return $hosts;
  }

  /**
   * Метод добавления текста в elasticsearch.
   * Возвращает id текста.
   * 
   * @param int $user_id
   * @param string $text
   * 
   * @return string
   */
  public function add(int $user_id, string $text)
  {
    $content = [
      'index' => 'telegram',
      'type' => 'telegram',
      'body' => [
        'user_id' => $user_id,
        'text' => $text
      ]
    ];

    try {
      $res = $this->client->index($content);
    } catch (\Exception $e) {
      return '';
    }

    return $res['_id'];
  }

  /**
   * Метод удаления текста из elasticsearch.
   * Возвращает id текста.
   * 
   * @param string $text_id
   * @param int $user_id
   * 
   * @return string 
   */
  public function remove(string $text_id, int $user_id)
  {
    $content = [
      'index' => 'telegram',
      'type' => 'telegram',
      'id' => $text_id
    ];

    if ($this->searchById($text_id)['user_id'] == $user_id) {
      $res = $this->client->delete($content);
      return $res['_id'];
    }

    return '';
  }

  /**
   * Поиск текста по слову/словосочетанию.
   * 
   * @param string $text
   * 
   * @return array
   */
  public function searchByText(string $text)
  {
    $params = [
      'index' => 'telegram',
      'type' => 'telegram',
      'body' => [
        'query' => [
          'match' => [
            'text' => $text
          ]
        ]
      ]
    ];

    $res = $this->client->search($params);

    $results = [];

    foreach ($res['hits']['hits'] as $item) {
      array_push($results, [
        'id' => $item['_id'],
        'user_id' => $item['_source']['user_id'],
        'text' => $item['_source']['text']
      ]);
    }

    return $results;
  }

  /**
   * Поиск текста по ID.
   * 
   * @param string $text_id
   * 
   * @return array
   */
  public function searchById(string $text_id)
  {
    $params = [
      'index' => 'telegram',
      'type' => 'telegram',
      'id' => $text_id
    ];

    try {
      $res = $this->client->get($params);
    } catch (\Exception $e) {
      return [];
    }

    $result = [
      'id' => $res['_id'],
      'user_id' => $res['_source']['user_id'],
      'text' => $res['_source']['text']
    ];

    return $result;
  }
}
