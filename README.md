Модуль для работы с Elasticsearch через телеграмм-бота
======================================================

### Установка ###

Выполните следующую команду в корневой папке проекта:
```terminal
composer require "coollemon/elasticsearch"
```

Или добавьте зависимость в файл composer.json
```json
{  ...
    "require": {
        "coollemon/elasticsearch": "^2.0.0"
    }, ...
}
```
А затем обновите ваш композер
```terminal
composer update
```

### Использование

В корневой папке создайте каталог public, и в нем файл bot.php со следующим содержимым
```php
<?php

require '../vendor/autoload.php';

use App\Telegram\TelegramBot;

$updates = json_decode(file_get_contents('php://input'), true);

$bot = new TelegramBot($updates, $token);
$bot->update();
```

В корневой папке создайте каталог config, и в нем файл conf.php со следующим содержимым
```php
<?php

return [
  'telegram_token' => '12345:your_token',
  'es_hosts' => [
    'localhost:9200',
    '127.0.0.1:9200'
  ]
];
```

Затем установите вебхук бота на bot.php
```url
https://api.telegram.org/bot12345:YourBotToken/setWebhook?url=https://your.host/public/bot.php
```

### Команды бота

Справка
```
/help
```
Поиск текста
```
search: new
```
Добавление текста
```
add: New text
```
Удаление текста
```
remove: textID
```